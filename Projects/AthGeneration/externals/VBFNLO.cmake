#
# File specifying the location of VBFNLO to use.
#

set( VBFNLO_LCGVERSION "3.0.0beta2p3" )
set( VBFNLO_LCGROOT
  "${LCG_RELEASE_DIR}/MCGenerators/vbfnlo/${VBFNLO_LCGVERSION}/${LCG_PLATFORM}" )